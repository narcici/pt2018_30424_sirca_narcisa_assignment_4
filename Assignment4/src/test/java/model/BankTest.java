package model;

import java.util.Calendar;

import junit.framework.TestCase;

public class BankTest extends TestCase {
	
	Bank bank=new Bank();
	Person p1= new Person("Ion Pop", "1453428594372");
	Person p2= new Person("Ioana Onofreiu", "1453428594370");
	Calendar cal = Calendar.getInstance();
	SpendingAccount ac1= new SpendingAccount(1, 100000, cal.getTime() );
	/**
	 *@Test
	 */	
	protected void setUp() throws Exception {
		super.setUp();
	}
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testAddPerson() {
		bank.addPerson(p1);
		bank.addPerson(p2);
		//assert(bank.getMapping().keySet().contains(p1));
	}
	public void addNullPerson() {
		bank.addPerson(null);
	}
	public void testRemovePerson() {
		bank.removePerson(p2.getCNP()); // remove person 2
	}
	public void testAddHolderAccount() {
		bank.addHolderAssocAccount(p1.getCNP(), ac1); // p1 ac1
	}
	public void testRemoveHolderAccount() {
		bank.removeHolderAssocAccount(p1.getCNP(), 1);
	}
}
