package controller;

import java.util.Calendar;

import model.*;

public class Main {


	public static void main(String[] args) {
		Bank bank= new Bank();
		/*
		Person p1= new Person("Ion Pop", "1453428594372");
		Person p2= new Person("Alina Ionescu","2453953282532");
		Person p3= new Person("Mircea Ionescu","2943753040126");
		Person p4= new Person("Daniel Hiris","1653422305953");
		
		bank.addPerson(p1);
		bank.addPerson(p2);
		bank.addPerson(p3);
		bank.addPerson(p4);
		Calendar cal = Calendar.getInstance();
		SpendingAccount ac1= new SpendingAccount(1, 100000, cal.getTime() ); // cal.getTime returns a Date class instance
		//System.out.println(cal.getTime());
		//System.out.println(ac1);
		bank.addHolderAssocAccount("1653422305953", ac1);
		
		SpendingAccount ac2 = new SpendingAccount(2, 12000, cal.getTime());
		bank.addHolderAssocAccount("2453953282532", ac2);
		SavingAccount ac3 = new SavingAccount(3, 5000, cal.getTime());
		bank.addHolderAssocAccount("2453953282532", ac3);
		
		SpendingAccount ac4 = new SpendingAccount(4, 12000, cal.getTime());
		bank.addHolderAssocAccount("2943753040126", ac4);
		SpendingAccount ac5 = new SpendingAccount(5, 12000, cal.getTime());
		bank.addHolderAssocAccount("1453428594372", ac5);
		
		//bank=bankSer;   // update bank
		//bank.removeHolderAssocAccount("2453953282532", 2);
		//bank.setMapping(mapHere);
		//ac5.deposit(25000);
		//System.out.println(bank.findbyCNP("2453953282532").getName());
		//System.out.println(mapHere.keySet());
		
		bank.serialization(bank);
		*/
		bank= bank.deserialization();
		Control c= new Control(bank);
		
	}
	
}
