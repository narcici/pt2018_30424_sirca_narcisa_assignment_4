package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import view.AccountsInterface;
import view.BankInterface;
import view.PersonInterface;


public class Control {		
	private ListSelectionModel listSelectionModelPersons;
	private ListSelectionModel listSelectionModelAccounts;
		Bank bank=new Bank();
		BankInterface bankInterface= new BankInterface();
		PersonInterface personInterface=  new PersonInterface();
		AccountsInterface accountsInterface=  new AccountsInterface();
		JTable tablePerson=new JTable();
		JTable tableAccount=new JTable();
		
	public Control(Bank bank) {
		//System.out.println(bank.getMapping());
		this.bank=bank;
		bankInterface.getFrame().setVisible(true);
		controlPerson(personInterface); // start control for this view
		controlAccount(accountsInterface); // start control for Account
		//showPersons();
		
		bankInterface.addActionListenerPersons(e->{
			Set<Person> set=bank.getMapping().keySet();
			System.out.println(set);
			ArrayList<Person> list = new ArrayList<Person>();
			SetJTable setter= new SetJTable();
			for(Person p: set) { // arraylist
				list.add(p);
			}
			System.out.println(list);
		//System.out.println(list.get(0));
			DefaultTableModel tableModel= new DefaultTableModel(setter.createStringMatrix(list), setter.makeTableHeader(list.get(0)));
			tablePerson.setModel(tableModel);
			personInterface.setTable(tablePerson); // put this created table into my view
			personInterface.setScrollPane(tablePerson);
			personInterface.getFrame().setVisible(true);
		});	
		bankInterface.addActionListenerAccounts(e->{
			accountsInterface.getFrame().setVisible(true);
			ArrayList<Account> listOfALL = new ArrayList<Account>(); // form the arraylist here
			Collection<ArrayList<Account>> acc= bank.getMapping().values(); // collection of ArrayLists
			for(ArrayList<Account> lista:acc) {
				for(Account cont:lista) {
				listOfALL.add(cont); // make a list of them all
				}
			}			
			String[][] accountsMatrix= accountsMatrix(listOfALL);
			String[] accountsHeader= accountssHeader();
			DefaultTableModel tableModel= new DefaultTableModel(accountsMatrix, accountsHeader);
			tableAccount.setModel(tableModel);			
			accountsInterface.setTable(tableAccount); // add this table to my view
			accountsInterface.setScrollPane(tableAccount);
			accountsInterface.getFrame().setVisible(true);
			}
		);
	}
	private  String[][] accountsMatrix(ArrayList<Account> listOfALL) {
		String[][] accountsMatrix= new String[20][4];
		int i=0;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		for(Account ac:listOfALL) {
			accountsMatrix[i][0]= Integer.toString(ac.getId());
			accountsMatrix[i][1]= Double.toString(ac.getTotalAmount());
			if(ac.getType()==0)
				accountsMatrix[i][2]="Spending Account";
			else
				accountsMatrix[i][2]="Saving Account";
			accountsMatrix[i][3]= df.format(ac.getCreateDate());
			i++;
		}
		return accountsMatrix;
		}
	private String[] accountssHeader() {
		String[] accountsHeader= new String[4]; //id amount type date
		accountsHeader[0]="id";
		accountsHeader[1]="totalAmount";
		accountsHeader[2]="type";
		accountsHeader[3]="date";
		return accountsHeader;
	}
	private void controlPerson(PersonInterface pi) {
		bank =bank.deserialization();
		pi.addInsertActionListener(e->{   // insert pers
			Person pers=new Person(pi.getPersonName(), pi.getPersonCNP());
			bank.addPerson(pers);
			bank.serialization(bank);
		}
		);
		pi.addDeleteActionListener(e->{   // delete pers
			bank.removePerson(pi.getPersonCNP());
			bank.serialization(bank);
		}
		);
		pi.addUpdateActionListener(e->{   // update pers
			String name=pi.getPersonName();
			String cnp=pi.getPersonCNP();
			bank.updatePerson(name, cnp); // update means change the name assoc with that cnp
			bank.serialization(bank);
		});
		pi.addListPersonsActionListener(e->{
			Set<Person> set=bank.getMapping().keySet();
			ArrayList<Person> list = new ArrayList<Person>();
			SetJTable setter= new SetJTable();
			for(Person p: set) {
				list.add(p);
			}
			DefaultTableModel tableModel= new DefaultTableModel(setter.createStringMatrix(list), setter.makeTableHeader(list.get(0)));
			tablePerson.setModel(tableModel);
			
			pi.setTable(tablePerson); // put this created table into my view
			pi.setScrollPane(tablePerson);
			pi.getFrame().setVisible(true);
			bank.serialization(bank);
		}
		);
		pi.addListAccountsActionListener(e->{
			pi.setAccounts(bank.generateReportForAccounts(pi.getPersonCNP()));
		});
		pi.addRaportActionListener(e->{
			bank.generateReportForClients(pi.getPersonCNP());
		});
		
		listSelectionModelPersons = tablePerson.getSelectionModel();
		tablePerson.setSelectionModel(listSelectionModelPersons);
        listSelectionModelPersons.addListSelectionListener(new SharedListSelectionHandler());
        tablePerson.setSelectionModel(listSelectionModelPersons);		
		bank.serialization(bank);
	}
	private void controlAccount(AccountsInterface ai) {
		bank =bank.deserialization();
		ai.addSpendingActionListener(e->{ // insert spending
			String cnp= ai.getPersonCNP();
			System.out.println(cnp);
			int id= ai.getAccountId();
			System.out.println(id);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, ai.getYear());
			cal.set(Calendar.MONTH, ai.getMonth());
			cal.set(Calendar.DAY_OF_MONTH, ai.getDay());
			Date dateRepresentation = cal.getTime();
			System.out.println(cal.getTime());
			SpendingAccount account= new SpendingAccount(id,0,dateRepresentation);
			System.out.println(account);
			// associate this new account with its holder
			bank.addHolderAssocAccount(cnp, account);
			bank.serialization(bank);
			
		});
	
		ai.addSavingActionListener(e->{ // insert saving
			String cnp= ai.getPersonCNP();
			int id= ai.getAccountId();
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, ai.getYear());
			cal.set(Calendar.MONTH, ai.getMonth());
			cal.set(Calendar.DAY_OF_MONTH, ai.getDay());
			Date dateRepresentation = cal.getTime();
			SavingAccount account= new SavingAccount(id,0,dateRepresentation);
			// associate this new account with its holder
			bank.addHolderAssocAccount(cnp, account);
			bank.serialization(bank);
	
		});
		
		ai.addDeleteActionListener(e->{ // delete acccount
			bank.removeHolderAssocAccount(ai.getPersonCNP(), ai.getAccountId());
			bank.serialization(bank);
		});
		ai.addInterestAcctionListener(e->{ // compute interest
			Account ac= bank.findHisAccount(ai.getPersonCNP(), ai.getAccountId());
			//System.out.println(ac);
			//System.out.println(ac.getTotalAmount());
			if(ac instanceof SavingAccount) {
			((SavingAccount) ac).composeInterest();
			//System.out.println(ac.getTotalAmount());
			}
			 bank.serialization(bank);
		});
		ai.addDepositAcctionListener(e->{  // make a deposit
			System.out.println(bank.findHisAccount(ai.getPersonCNP(),ai.getAccountId()));
			System.out.println(ai.getAmount());
			//Account temp= 
			bank.findHisAccount(ai.getPersonCNP(),ai.getAccountId()).deposit(ai.getAmount());
			//System.out.println(temp);
			//temp
			bank.serialization(bank);
		});
		ai.addWithdrawAcctionListener(e->{ // withdraw
			bank.findHisAccount(ai.getPersonCNP(),ai.getAccountId()).withdraw(ai.getAmount());
			bank.serialization(bank);
		});
		ai.addUpdateAcctionListener(e->{ // withdraw
			bank.updateAccount(ai.getPersonCNP(), ai.getAccountId(), ai.getAmount());
			if(bank.findHisAccount(ai.getPersonCNP(), ai.getAccountId()).getType() == 1)
				JOptionPane.showMessageDialog(ai.getFrame(), " Saving accounts can not be updated!", "Warning", JOptionPane.WARNING_MESSAGE);

			bank.serialization(bank);
		});
		ai.addListAccountsActionListener(e->{
			// here comes the JTable thing
			ArrayList<Account> listOfALL = new ArrayList<Account>(); // form the arraylist here
			Collection<ArrayList<Account>> acc= bank.getMapping().values(); // collection of ArrayLists
			for(ArrayList<Account> lista:acc) {
				for(Account cont:lista) {
				listOfALL.add(cont); // make a list of them all
				}
			}			
			String[][] accountsMatrix= accountsMatrix(listOfALL);
			String[] accountsHeader= accountssHeader();
			//JTable table = new JTable();
			DefaultTableModel tableModel= new DefaultTableModel(accountsMatrix, accountsHeader);
			tableAccount.setModel(tableModel);			
			accountsInterface.setTable(tableAccount); // add this table to my view
			accountsInterface.setScrollPane(tableAccount);
			 bank.serialization(bank);
		});
		listSelectionModelAccounts = tableAccount.getSelectionModel();
		tableAccount.setSelectionModel(listSelectionModelAccounts);
		listSelectionModelAccounts.addListSelectionListener(new SharedListSelectionHandlerA());
        tableAccount.setSelectionModel(listSelectionModelAccounts);		
        bank.serialization(bank);
	}
	public void setBank(Bank bank) {
		this.bank= bank;
	}

	class SharedListSelectionHandlerA implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel) e.getSource();
            //JTable table1=table;
            if (tableAccount.getSelectedRow() != -1)
                {accountsInterface.getTextFieldID().setText(tableAccount.getValueAt(tableAccount.getSelectedRow(), 0).toString());
               // System.out.println(tableAccount.getValueAt(tableAccount.getSelectedRow(), 0).toString());
                }
        }
    }
	
	class SharedListSelectionHandler implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel) e.getSource();
            //JTable table1=table;
            if (tablePerson.getSelectedRow() != -1)
                { accountsInterface.getTextFieldCNP().setText(tablePerson.getValueAt(tablePerson.getSelectedRow(), 1).toString());
                personInterface.getCNPfield().setText(tablePerson.getValueAt(tablePerson.getSelectedRow(), 1).toString());
                //System.out.println(tablePerson.getValueAt(tablePerson.getSelectedRow(), 1).toString());
                }
        }
    }
	}