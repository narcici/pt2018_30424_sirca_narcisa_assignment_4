package view;
/*
 * FOR INPUT TESTING
 * https://stackoverflow.com/questions/8634139/phone-validation-regex?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 * https://www.geeksforgeeks.org/java-program-check-valid-mobile-number/
 */
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.MouseListener;

public class AccountTry {

	private JFrame frame;
	private JTextField textFieldCNP;
	public JButton btnSpendingACCOUNT;
	public JButton btnInsertSavingacc;
	public JButton btnUpdate;
	public JButton btnDelete;
	
	public JButton btnWithdraw;
	public JButton btnDeposit;
	
	
	private JTextField txtID;
	private JTextField txtYear;
	private JTextField txtMonth;
	private JTextField txtDay;
	private JLabel lblPersonEditHere;
	private JTable table; // here table
	private JTextField amount;
	private JScrollPane scrollPane;
	JButton btnUpdateForSaving;
	JButton btnListAccounts;
	

	public AccountTry() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 730);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCNP = new JLabel("CNP");
		lblCNP.setBounds(12, 63, 126, 16);
		frame.getContentPane().add(lblCNP);
		
		textFieldCNP = new JTextField();
		textFieldCNP.setBounds(12, 85, 116, 22);
		frame.getContentPane().add(textFieldCNP);
		textFieldCNP.setColumns(10);
		
		btnSpendingACCOUNT = new JButton("INSERT SPENDING acc");
		btnSpendingACCOUNT.setBounds(207, 84, 165, 25);
		frame.getContentPane().add(btnSpendingACCOUNT);
		
		btnInsertSavingacc = new JButton("INSERT SAVING acc");
		btnInsertSavingacc.setBounds(207, 130, 165, 25);
		frame.getContentPane().add(btnInsertSavingacc);
		
		btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(207, 175, 165, 25);
		frame.getContentPane().add(btnUpdate);
		
		btnDelete = new JButton("REMOVE");
		btnDelete.setBounds(207, 251, 165, 25);
		frame.getContentPane().add(btnDelete);
		
		JLabel lblAccountId = new JLabel("Account ID");
		lblAccountId.setBounds(12, 130, 92, 16);
		frame.getContentPane().add(lblAccountId);
		
		txtID = new JTextField();
		txtID.setBounds(12, 149, 116, 22);
		frame.getContentPane().add(txtID);
		txtID.setColumns(10);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(12, 184, 56, 16);
		frame.getContentPane().add(lblYear);
		
		txtYear = new JTextField();
		txtYear.setBounds(12, 203, 116, 22);
		frame.getContentPane().add(txtYear);
		txtYear.setColumns(10);
		
		JLabel lblMonth = new JLabel("Month");
		lblMonth.setBounds(12, 232, 56, 16);
		frame.getContentPane().add(lblMonth);
		
		txtMonth = new JTextField();
		txtMonth.setBounds(12, 252, 116, 22);
		frame.getContentPane().add(txtMonth);
		txtMonth.setColumns(10);
		
		txtDay = new JTextField();
		txtDay.setBounds(12, 305, 116, 22);
		frame.getContentPane().add(txtDay);
		txtDay.setColumns(10);
		
		JLabel lblDay = new JLabel("Day");
		lblDay.setBounds(12, 287, 56, 16);
		frame.getContentPane().add(lblDay);
		
		lblPersonEditHere = new JLabel("Account Edit Here");
		lblPersonEditHere.setBounds(149, 13, 126, 16);
		frame.getContentPane().add(lblPersonEditHere);
		
		// HERE IS the table of Accounts
		table = new JTable();
		//table.setBounds(12, 412, 408, 258);
		frame.getContentPane().add(table);
		
		JLabel lblHereTheAmount = new JLabel("Here the Amount fo money");
		lblHereTheAmount.setBounds(12, 340, 154, 16);
		frame.getContentPane().add(lblHereTheAmount);
		
		amount = new JTextField();
		amount.setBounds(12, 360, 116, 22);
		frame.getContentPane().add(amount);
		amount.setColumns(10);
		
		btnDeposit = new JButton("DEPOSIT ");
		btnDeposit.setBounds(207, 283, 165, 25);
		frame.getContentPane().add(btnDeposit);
		
		btnWithdraw = new JButton("WITHDRAW");
		btnWithdraw.setBounds(207, 321, 165, 25);
		frame.getContentPane().add(btnWithdraw);
		
		scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		/*table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		}); */
		scrollPane.setBounds(12, 390, 450, 400);
		frame.getContentPane().add(scrollPane);
		
		btnUpdateForSaving = new JButton("UPDATE FOR SAVING");
		btnUpdateForSaving.setBounds(207, 359, 165, 25);
		frame.getContentPane().add(btnUpdateForSaving);
		
		btnListAccounts = new JButton("LIST ACCOUNTS");
		btnListAccounts.setBounds(207, 213, 165, 25);
		frame.getContentPane().add(btnListAccounts);
		
		}

	public JFrame getFrame() {
		return frame;
	}
	public JTextField getTextFieldCNP() {
		return textFieldCNP;
	}
	public void setTextFieldCNP(JTextField textFieldCNP) {
		this.textFieldCNP = textFieldCNP;
	}
	public void setIdField(String s) {
		this.txtID.setText(s);
	}
	public JTextField getTextFieldID() {
		return txtID;
	}
	public double getAmount() {
		double amountS=0;
		try {
			 amountS=Double.parseDouble(amount.getText());
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid amount!", "Warning", JOptionPane.WARNING_MESSAGE);
		}
		return amountS;
	}
	public void setScrollPane(JTable t) {
		this.scrollPane.setViewportView(t);
	}
	public void setTable(JTable t) {
		this.table=t;
	}
	public String getPersonCNP() {
		String s=textFieldCNP.getText();
		Pattern p = Pattern.compile("[0-9]{13}");
		 Matcher m = p.matcher(s);
		 if (m.find() && m.group().equals(s))
			 return s;
		 else {
				JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid CNP!", "Warning", JOptionPane.WARNING_MESSAGE);
				return null;
		 }
	}
	public int getAccountId() {
		int id=0;
		try {
			id=Integer.parseInt(txtID.getText());
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid account id!", "Warning", JOptionPane.WARNING_MESSAGE);

		}
		return id;
	}
	public int getYear() {
		int year=0;
		try {
			year=Integer.parseInt(txtYear.getText());
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid year!", "Warning", JOptionPane.WARNING_MESSAGE);

		}
		if(year<2000 && year>2018)
			JOptionPane.showMessageDialog(getFrame(), "Please, recheck the year!", "Warning", JOptionPane.WARNING_MESSAGE);

		return year;
		}
	public int getMonth() {
		int month=0;
		try {
			month=Integer.parseInt(txtYear.getText());
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid month!", "Warning", JOptionPane.WARNING_MESSAGE);

		}
		if(month<0 && month>12)
			JOptionPane.showMessageDialog(getFrame(), "Please, recheck the month!", "Warning", JOptionPane.WARNING_MESSAGE);

		return month;	
		}
	public int getDay() {
		int day=0;
		try {
			day=Integer.parseInt(txtYear.getText());
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid day!", "Warning", JOptionPane.WARNING_MESSAGE);

		}
		if(day<0 && day>31)
			JOptionPane.showMessageDialog(getFrame(), "Please, recheck the day!", "Warning", JOptionPane.WARNING_MESSAGE);

		return day;	
		}	
	
	public void addSpendingActionListener(ActionListener a) { // insert
		btnSpendingACCOUNT.addActionListener(a);
	}
	public void addSavingActionListener(ActionListener a) { // insert
		btnInsertSavingacc.addActionListener(a);
	}
	public void addDeleteActionListener(ActionListener a) { // delete
		btnDelete.addActionListener(a);
	}
	public void addDepositAcctionListener(ActionListener a) { // deposit
		btnDeposit.addActionListener(a);
	}
	public void addWithdrawAcctionListener(ActionListener a) { // withdraw
		btnWithdraw.addActionListener(a);
	}
	public void addInterestAcctionListener(ActionListener a) { // interest
		btnUpdateForSaving.addActionListener(a);
	}
	public void addUpdateAcctionListener(ActionListener a) { // update
		btnUpdate.addActionListener(a);
	}
	public void addListAccountsActionListener(ActionListener a) {
		btnListAccounts.addActionListener(a);
	}

	public void addEventListener(MouseListener l) {
		table.addMouseListener(l);
	}
	
	public JTable getTable() {
		return table;
	}
}