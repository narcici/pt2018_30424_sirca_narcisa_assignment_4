package view;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JTable;

public class PersonTryyy {

	private JFrame frame;
	private JTextField personName;
	private JTextField textFieldCNP;
	public JButton btnInsert;
	public JButton btnUpdate;
	public JButton btnDelete;
	public JButton btnListAccounts;
	public JButton btnListpersons;
	private JTable table; // table of persons
	private JTextField hisAccounts; // set it to list the accounts of this person 
	private JLabel lblThisPersonsAccounts;
	private JScrollPane scrollPane;
	private JButton btnGenerateReport;
	
	public PersonTryyy() {
		initialize();
	}
	
	

	public JTextField getTextFieldCNP() {
		return textFieldCNP;
	}



	public void setTextFieldCNP(JTextField textFieldCNP) {
		this.textFieldCNP = textFieldCNP;
	}



	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 605);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPersonName = new JLabel("Name");
		lblPersonName.setBounds(12, 37, 116, 16);
		frame.getContentPane().add(lblPersonName);
		
		personName = new JTextField();
		personName.setBounds(12, 55, 159, 22);
		frame.getContentPane().add(personName);
		personName.setColumns(10);
		
		JLabel lblCNP = new JLabel("CNP");
		lblCNP.setBounds(12, 90, 126, 16);
		frame.getContentPane().add(lblCNP);
		
		textFieldCNP = new JTextField();
		textFieldCNP.setBounds(12, 119, 159, 22);
		frame.getContentPane().add(textFieldCNP);
		textFieldCNP.setColumns(10);
		
		btnInsert = new JButton("INSERT");
		btnInsert.setBounds(229, 33, 143, 25);
		frame.getContentPane().add(btnInsert);
		
		btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(229, 86, 143, 25);
		frame.getContentPane().add(btnUpdate);
		
		btnDelete = new JButton("REMOVE");
		btnDelete.setBounds(229, 124, 143, 25);
		frame.getContentPane().add(btnDelete);
		
		btnListpersons = new JButton("LIST PERSONS");
		btnListpersons.setBounds(229, 175, 143, 25);
		frame.getContentPane().add(btnListpersons);
		
		btnListAccounts = new JButton("LIST ACCOUNTS");
		btnListAccounts.setBounds(12, 163, 159, 25);
		frame.getContentPane().add(btnListAccounts);
		
		table = new JTable();
		table.setBounds(12, 287, 408, 258);
		frame.getContentPane().add(table);
		
		hisAccounts = new JTextField();
		hisAccounts.setBounds(12, 233, 202, 22);
		frame.getContentPane().add(hisAccounts);
		hisAccounts.setColumns(10);
		
		lblThisPersonsAccounts = new JLabel("This persons Accounts: id +amount");
		lblThisPersonsAccounts.setBounds(12, 211, 202, 16);
		frame.getContentPane().add(lblThisPersonsAccounts);
		
		scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(12, 260, 450, 400);
		frame.getContentPane().add(scrollPane);
		
		btnGenerateReport = new JButton("GENERATE REPORT");
		btnGenerateReport.setBounds(229, 232, 177, 25);
		frame.getContentPane().add(btnGenerateReport);
	}

	public void setScrollPane(JTable t) {
		this.scrollPane.setViewportView(t);
	}

	public JFrame getFrame() {
		return frame;
	}
	public void setTable(JTable t) {
		this.table=t;
	}

	public String getPersonName() {
		return personName.getText();
	}

	public String getPersonCNP() {
		String s=textFieldCNP.getText();
		Pattern p = Pattern.compile("[0-9]{13}");
		 Matcher m = p.matcher(s);
		 if (m.find() && m.group().equals(s))
			 return s;
		 else {
				JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid CNP!", "Warning", JOptionPane.WARNING_MESSAGE);
				return null;
		 }	}
	public void setCNPfield(String s) {
		textFieldCNP.setText(s);
	}
	public JTextField getCNPfield() {
		return textFieldCNP;
	}
	public void setAccounts(String s) {
		hisAccounts.setText(s);
	}
	public void addInsertActionListener(ActionListener a) { // insert
		btnInsert.addActionListener(a);
	}
	public void addDeleteActionListener(ActionListener a) { // delete
		btnDelete.addActionListener(a);
	}
	public void addUpdateActionListener(ActionListener a) { // update
		btnUpdate.addActionListener(a);
	}
	public void addListPersonsActionListener(ActionListener a) { // listPersons
		btnListpersons.addActionListener(a);
	}
	public void addListAccountsActionListener(ActionListener a) { // listAccounts
		btnListAccounts.addActionListener(a);
	}

	public JTable getTable() {
		return table;
	}
}