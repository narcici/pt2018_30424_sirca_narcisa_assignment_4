package view;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class BankInterface {

	private JFrame frame;
	public  JButton btnPersons;
	public  JButton btnAccounts;
	
	public BankInterface() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 530, 485);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel topLabel = new JLabel("Bank Management");
		topLabel.setFont(new Font("Serif", Font.BOLD, 26));
		topLabel.setBounds(158, 100, 250, 40);
		frame.getContentPane().add(topLabel);
		
		btnAccounts = new JButton("Accounts");
		btnAccounts.setBounds(88, 257, 97, 25);
		frame.getContentPane().add(btnAccounts);
		
		btnPersons = new JButton("Persons");
		btnPersons.setBounds(339, 257, 97, 25);
		frame.getContentPane().add(btnPersons);
		
		
	}

	public JFrame getFrame() {
		return frame;
	}
	public void addActionListenerPersons(ActionListener a) {
		btnPersons.addActionListener(a);
	}
	public void addActionListenerAccounts(ActionListener a) {
		btnAccounts.addActionListener(a);
	}
}
