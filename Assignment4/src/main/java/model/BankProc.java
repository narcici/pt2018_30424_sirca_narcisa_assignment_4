package model;

public interface BankProc {
	/**
	 * @pre p!=null
	 * @pos mapping.keySet().size = mapping.get(p).size@pre+1 
	 * @param p
	 */
	void addPerson(Person p);
	
	/**
	 * @pre cnp!=null
	 * @pos @nochange || mapping.keySet().size = mapping.get(p).size@pre-1 
	 * @param cnp
	 */
	void removePerson(String cnp);
	/**
	 * @pre cnp!=null && a!=null
	 * @pos @nochange || mapping.get(fingbyCNP(cnp)).size() = mapping.get(fingbyCNP(cnp)).size()@pre +1
	 * @param cnp
	 * @param a
	 */
	void addHolderAssocAccount(String cnp, Account a);  // here method to add holder associated accounts
	/**
	 *@pre cnp!=null
	 *@pre idAccount>0
	 *@pre mapping.get(fingbyCNP(cnp)).size() = mapping.get(fingbyCNP(cnp)).size()@pre -1
	 * @param cnp
	 * @param idAccount
	 */
	void removeHolderAssocAccount(String cnp, int idAccount); // need parameters hereee
	/**
	 * @pre cnp!=null
	 * @post @nochange
	 * @param cnp
	 */
	void generateReportForClients(String cnp);//generate a report specifying  all persons and their number of accounts
	/**
	 *@pre cnp!=null
	 *@pos @nochange
	 *@param cnp
	 *@return String
	 */
	String generateReportForAccounts(String cnp); // no idea really
		/**
	 * @pre bank!=null
	 * @pos
	 * @param bank
	 */
	public void serialization(Bank bank);
	/**
	 * @pre bank.ser != null
	 * @return Bank
	 */
	public Bank deserialization();
}
