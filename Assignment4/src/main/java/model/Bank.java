package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@SuppressWarnings("serial")
public class Bank implements BankProc, Serializable{
	
	private HashMap<Person, ArrayList<Account>> mapping; // keep a hashmap 
		public Bank() {
		mapping = new HashMap<Person, ArrayList<Account>>(30); // initial capacity=30
	}
	public HashMap<Person, ArrayList<Account>> getMapping(){  // not in interface
		return mapping;
	}
	public void setMapping(HashMap<Person, ArrayList<Account>> mapHere) { // not in interface
		this.mapping=mapHere;
	}
	public void addPerson(Person p) {
		int nr=mapping.keySet().size();
		//assert isWellFormed();
		assert p!=null;
		if (mapping.containsKey(p) != true) {
			mapping.put(p, new ArrayList<Account>()); //adding a person with no accounts
			//System.out.println(mapping.keySet()); // all the persons
		}
		assert mapping.keySet().size()== nr +1;
		//assert isWellFormed();
	}
	public void removePerson(String cnp) {
		assert cnp!=null;
		//assert isWellFormed();
		int nrOfPers=mapping.keySet().size();
		HashMap<Person, ArrayList<Account>> map= getMapping();
		Person pers = findbyCNP(cnp);
		if(pers != null) {	
			mapping.remove(pers);
			assert mapping.keySet().size()==nrOfPers-1;
			}
		else assert map== getMapping(); //here no change was done
		//assert isWellFormed();
	}
	public void addHolderAssocAccount(String cnp, Account a) {
		assert cnp!=null;
		assert a!=null;
		//assert isWellFormed();
		Person pers = findbyCNP(cnp);
		if(pers != null) {
					int noAccounts=mapping.get(pers).size();
						mapping.get(pers).add(a); // add a in person's list
						pers.setAccountsNo(pers.getAccountsNo() + 1);
						a.addObserver(pers); // this person is observer for this account
						assert mapping.get(pers).size() == noAccounts+1;
					}
		else {
			
		}
		//assert isWellFormed();
		}
	public void removeHolderAssocAccount(String cnp, int idAccount) { // remove only an account of this pers
		assert isWellFormed();
		Person pers = findbyCNP(cnp);
		if(pers != null) {
				int noAccounts=mapping.get(pers).size();
				ArrayList<Account> acc= mapping.get(pers); // value of the arraylist mapped with pers
				for(Account a:acc) 
					if(a.getId() == idAccount) {
						mapping.get(pers).remove((Account)a); //. remove
						pers.setAccountsNo(pers.getAccountsNo() - 1); // dec
						assert noAccounts-1 ==mapping.get(pers).size(); // size decreased by 1 
				}
					else assert  acc== mapping.get(pers); // means no change, same list
		}
		assert isWellFormed();
	}	
	public void updateAccount(String cnp, int idAccount, double amount) { // update the amount // set it
		Account s=findHisAccount(cnp, idAccount);
		s.setTotalAmount(amount); //identify account
	}
	public void updatePerson(String name, String cnp) {
		findbyCNP(cnp).setName(name);		
	}
	public Person findbyCNP(String cnp) {
		Set<Person> set=mapping.keySet(); // get the set of key values 'cause I need to search for one
		for(Person pers:set) 
			if (pers.getCNP().equals(cnp))
			{
				return pers;
			}
		return null;
	}
	public Account findHisAccount(String cnp,int idAccount) {
		Person pers = findbyCNP(cnp);
		if(pers != null) {	
				ArrayList<Account> acc= mapping.get(pers);
				for(Account a:acc) 
					if(a.getId() == idAccount) {
						return a;
					}
			}
		return null;
	}
	public String generateReportForAccounts(String cnp) {
		assert cnp!=null;
		//assert isWellFormed();
		Person pers= findbyCNP(cnp);
		if(pers != null) {
		ArrayList<Account> acc= mapping.get(pers);
		StringBuffer buf = new StringBuffer(pers.getName()+" ");
		if (acc!= null) 
			for(Account a:acc) {
			buf.append(a.toString()+" ");
		}
		return buf.toString();
		}
		else return "no person found at this cnp";
	}
	public void generateReportForClients(String cnp) { // writes the detail about it in console
		assert cnp!=null;
		Person p=findbyCNP(cnp);
		if(p!=null) {
			ArrayList<Account> acc=mapping.get(p);
			System.out.println(p.getName()+" holds the following accounts");
			for(Account a:acc) {
				System.out.println(a.toString());
			}
		}
	}
	public boolean isWellFormed() {
		Set<Person> set= mapping.keySet();
		for(Person p:set){
			if (mapping.get(p) == null)
				return false;
				}
		return true;
	}
	public void serialization(Bank bank) { // Write the bank data into a binary file
			try {
				FileOutputStream fs= new FileOutputStream("bank.ser");
				ObjectOutputStream os = new ObjectOutputStream(fs);
				os.writeObject(bank);
				os.close();
				fs.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}	
	public Bank deserialization(){ //reading the bank data from a binary file here
			Bank bank=new Bank();
			try {
				FileInputStream fs=new FileInputStream("bank.ser");
				ObjectInputStream os= new ObjectInputStream(fs);
				Object bankObj= os.readObject();
				bank= (Bank)bankObj;
				os.close();
				fs.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
			return bank;
		}
}
