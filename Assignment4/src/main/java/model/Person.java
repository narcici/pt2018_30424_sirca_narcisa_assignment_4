package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;


@SuppressWarnings("serial")
public class Person implements Observer, Serializable {
	private String name;
	private String CNP; // CNP is string or some long value??
	private int accountsNo=0; // increment for the number of accounts this person holds, for report 
	
	public Person(String name,String cnp) {
		this.name=name;
		this.CNP=cnp;
		accountsNo=0; //at this point he has no accounts
	}
	public Person() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCNP() {
		return CNP;
	}
	public void setCNP(String cNP) {
		CNP = cNP;
	}
	public int getAccountsNo() {
		return accountsNo;
	}
	public void setAccountsNo(int accountsNo) {
		this.accountsNo = accountsNo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CNP == null) ? 0 : CNP.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (CNP == null) {
			if (other.CNP != null)
				return false;
		} else if (!CNP.equals(other.CNP))
			return false;
		return true;
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		//System.out.println("modification here");
		System.out.println(this.getName()+" "+(String)arg1); // I'll notify my observer by printing at console the modification that has been made
	}
}
