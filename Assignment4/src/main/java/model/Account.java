package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class Account extends Observable implements Serializable {
	Observer obs;
	int id; // must differentiate it somehow
	double totalAmount; // the total amount for money this  holder has
	Date createDate;
	int type;
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	
	public Account(int id, double amount, Date date) { // aci data o pun eu
		this.id = id;
		this.totalAmount = amount; // usually 0
		this.createDate= date; 
		this.type=0;
		//notifyObservers("Account "+id+" was created at "+df.format(createDate)); // add date here
		//clearChanged();
	}
	public Account() {
		id=0;
		totalAmount=0;
		createDate= null;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getType() {
		return type;
	}
 	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
		//setChanged();
		notifyObservers(id+": Update account, now the initial date is "+createDate.toString());
		//clearChanged(); // all observers had been notified
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
		//setChanged();
		notifyObservers(id+": Your account total amount of money has been set as "+totalAmount);
		//clearChanged();
	}
	public int getId() {
		return id;
	}
	public void deposit(double amount) {
		totalAmount= totalAmount + amount;
		setChanged();
		notifyObservers(id+": This amount of money was deposited "+amount);
		clearChanged();
	}
	public void withdraw(double amount) {
		if (totalAmount >= amount) {
			totalAmount= totalAmount - amount;
			//setChanged();
			notifyObservers(id+": This amount of money was withdrawn "+amount);
			//clearChanged();
		}
		else {
			//setChanged();
			notifyObservers(id+": Not enough money! the amount "+amount +" could not be withdrawn from the total of "+totalAmount);
			//clearChanged(); // it is needed??
		}	
	}
	@Override
	public String toString() {
		String s;
		if(type==0) {
			s="Spending Account";
		}
		else s="Saving Account";
		return id + " totalAmount:" + totalAmount+"type: "+s+" date: "+df.format(createDate);
	}
	@Override
	public void addObserver(Observer o) {
		this.obs=o;
	}
	@Override
	public void notifyObservers(Object arg) {
		setChanged();
		obs.update(this, arg);
		//System.out.println((String)arg);
		  }
	
	
}
