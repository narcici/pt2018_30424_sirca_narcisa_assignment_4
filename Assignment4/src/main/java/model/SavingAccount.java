package model;

import java.io.Serializable;
import java.util.Date;
/*
 * class having the possibility to make one deposit of an large amount of money
 * and one withdrawal, of the complete amount
 * it questions the deposit period
 */

@SuppressWarnings("serial")
public class SavingAccount extends Account implements Serializable {
	double interest=0.2; // rate to compose the interest for this sum
	boolean depositWasMade;
	boolean withdrawWasMade;
	
	public SavingAccount(int id, double amount, Date date) {
		super(id, 0, date);
		setType(1);
		depositWasMade= false;
		withdrawWasMade= false;
		
	}
	@Override
	public void setTotalAmount(double amount) {
		notifyObservers(id+" Saving accounts can not be updated!");
	}
	@Override
	public void deposit(double amount) { // boolean type so I can check if it is performed
		if(depositWasMade ==false) {
			totalAmount=amount;
			notifyObservers("Saving account "+id+" now has the amount:"+amount);
			depositWasMade= true;
			withdrawWasMade=false;
		}
		else {
			notifyObservers("Saving account "+id+" can not receive another deposit");
		}
	}
	@Override
	public void withdraw(double amount) {
		if(withdrawWasMade == false) {
			totalAmount=0;
			notifyObservers("Saving account "+id+" is now empty!");
			depositWasMade=false;
			withdrawWasMade=true;
		}
	}
	public void composeInterest() {//just replace the amount value

			totalAmount= (1+interest)* getTotalAmount(); // increase the value by 20%
			notifyObservers("Saving account "+id+" interest was composed, new amount is "+getTotalAmount());	
	}
}
