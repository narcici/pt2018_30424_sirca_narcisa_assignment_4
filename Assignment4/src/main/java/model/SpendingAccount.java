package model;

import java.util.Date;

/*
 * here's a class that can have several deposits and several withdrawals, and doesn't any interest
 */
@SuppressWarnings("serial")
public class SpendingAccount extends Account{

	public SpendingAccount(int id, double amount, Date date) {
		super(id, amount, date);
	}

}
